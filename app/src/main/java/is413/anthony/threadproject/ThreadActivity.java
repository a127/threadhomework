package is413.anthony.threadproject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import butterknife.ButterKnife;
import butterknife.InjectView;


public class ThreadActivity extends Activity
{
    @InjectView(R.id.input_text)
    EditText input_text;
    @InjectView(R.id.progress_text)
    TextView progress_text;
    @InjectView(R.id.submit_button)
    Button submit_button;

    private Integer   temp_input;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);

        ButterKnife.inject(this);

        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                temp_input = (Integer.parseInt(input_text.getText().toString()));
                LoadingTask loadingTask = new LoadingTask();
                loadingTask.execute(temp_input);
            }
        });

    }

    private class LoadingTask extends AsyncTask<Integer, Integer, Double>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();

        }

        @Override
        protected Double doInBackground(Integer... params)
        {
            int temp = params[0];

            for(int i = 0; i < 101; i++)
            {
                try
                {
                    Thread.sleep(100);
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }

                publishProgress(i);

            }

            return Math.floor((temp - 32) * .556);

        }

        @Override
        protected void onProgressUpdate(Integer... values)
        {
            super.onProgressUpdate(values);

            progress_text.setText("Loading..." + values[0] + "%");

        }

        @Override
        protected void onPostExecute(Double aDouble)
        {
            super.onPostExecute(aDouble);

            progress_text.setText(aDouble + "C°");

        }

    }

}
